clear
import delimited "C:\Users\bwild\OneDrive\data-challenge\data\emergency_mortality.csv"

encode race, gen(race_factor)

fvset base 4 race_factor
logistic deceased income i.race_factor