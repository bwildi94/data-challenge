# Data tasks answers

The answers to the data task questions are all provided in the Answers.pdf file. Python code is contained in Part1.ipynb, and code to create the regression is in regression.do.
The original data files, altered data files, and manually added data files are all in the data folder.