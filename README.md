# Laboratory for Systems Medicine

We have a somewhat unusual selection process for positions in the lab. Most notably, **we don't do job interviews** to decide whom to hire, because of evidence that interviews are at best [noise](http://journal.sjdm.org/12/121130a/jdm121130a.pdf) and at worst [biased](https://onlinelibrary.wiley.com/doi/full/10.1111/j.1744-6570.1982.tb02197.x).

We'd rather choose based on what people can do and have done, rather than on how well they can answer questions in a short, high pressure situation. Besides, as students, we always preferred take home, open book exams over in class ones.

Instead, our process looks like this.
1. We screen out the majority of applicants on the basis of **CV and transcript** alone -- so if you've made it to this phase, congratulations! We were really impressed by your materials.
1. We'd like to ask you to complete the **data challenge** described below. We've put a lot of thought into designing a task that allows us to look for the kind of skills that will make for a great match for you and us.

We take this process seriously, and would rather give you a chance to do your best work this way. We only ask very few people to do the task, so our expectation is that you'll do well at it.


## Data Challenge
We have provided a modified synthetic dataset from [SyntheticMass](https://syntheticmass.mitre.org/index.html). These data resemble those we work with every day.

There are two parts to the challenge:
- **Part 1**: These questions are required, and simulate tasks often needed in the early phases of projects.
- **Part 2**: Choose 1 of 4 questions in line with your interests and strengths. As a multi-disciplinary lab, we want you to showcase your talents!

Please see below for more information on what we're looking for in the answers.

### Part 1: Basics (Please Answer All)

1. **Describe the data** (i.e. what information is provided in the dataset?), in a few sentences. What does it appear we've been given?

1. **Summary statistics**. Create a sample of patients with emergency visits between from 2008-2016. Most medical papers begin with a "Table 1" that summarizes basic aspects of the sample (see this [paper](https://www.bmj.com/content/359/bmj.j5468) for an example). Create a similar "Table 1" that describes basic attributes of this sample, including *n* (visits and patients), demographics (age, sex, race), and the medical conditions diagnosed over the year prior to the emergency visits. We don't have the exact same illness categories as in the linked paper, so please substitute rows for the most common diagnoses you find in the data. If anything needs further explanation, just put a footnote in the table.

1. **Run a simple model**. We're interested in the relationship between mortality after these visits, and income and race. To (roughly) measure income, we'll use zip-code level median household income in the past 12 months from the [2012-2016 ACS](https://factfinder.census.gov/faces/tableservices/jsf/pages/productview.xhtml?pid=ACS_16_5YR_S1901&prodType=table)-- oh, and please go back and add this variable to your "Table 1 above." Then, using regression, model mortality in the year after emergency visits as a function of `income, race` and present the model output. You can use these [ugly screenshots](https://dss.princeton.edu/online_help/analysis/interpreting_regression.htm) as a general guide for what key information to present, but you don't need to replicate it exactly or present all the same information.


### Part 2: Applications $`{4}\choose{1}`$

1. Imagine you are partnering with a large hospital network (comprising 15 hospitals) and your task is to develop a model that predicts how many emergency visits they will have on any given day. They are giving you access to all the historical emergency department data from 3 of their hospitals spanning 2008-2016. They hope to implement your model across the entire network of hospitals for the upcoming year. In a few sentences, take us through your thoughts about setting up a process to build and evaluate your model. You do not need to talk about specifics (e.g. you don't need to talk about which kind of algorithm you would use) -- just your high-level strategy for the design of the algorithm.

1. Consider a particular medical condition with $`\frac{1}{12}`$ prevalence in the population. There are two tests for this condition and doctors randomly choose test 1 with probability $`\frac{1}{3}`$ and test 2 with probability $`\frac{2}{3}`$. If a given individual has the condition, test 1 would correctly identify this with probability $`1`$ and test 2 would correctly identify this with probability $`\frac{5}{6}`$. If a given individual does not have the condition, test 1 correctly identifies this with probability $`\frac{1}{2}`$ and test 2 correctly identifies this with probability $`\frac{3}{4}`$. Imagine an individual is chosen completely at random from the population, they receive one of the tests, and the test comes out positive. What is the probability that the individual actually has the disease?

1. Build a web application for a patient to view his/her medical records. The patient should be able to view all of the information provided in the `/data` files.

1. If none of these are speaking to you, please feel free to investigate your own research question using the provided data! You are welcome to pursue any question you like and use any methods you find suitable. The goal should not be to find something statistically significant or something that would be publishable -- we just want you to take us through your thought process when pursuing a new research question.

## What We Are Looking For
Great research requires creativity, judgment, insight and a host of other qualities; but all of these build on a foundation of meticulousness. In this task we're looking for that foundation - extreme attention to detail.

- Code
    - We value (and try to write) clean, well-documented, reproducible code. We share and re-use code all the time, so other people should be able to read and understand what you've written.
    - Bonus points for efficient and modular code since we typically work with much larger data than you see here and iterate on problems.
- Thought Process
    - Walking us through how you arrived at your results is extremely valuable. Include assumptions and technical details where appropriate, but focus on communicating your insights in a clear and intuitive way. We hope to further discuss your findings with you!
- Time Commitment
    - You are welcome to spend as little or as much time as you'd like on the project. However, we anticipate that this will take roughly as long as a standard problem set from one of your quantitative courses.
- Additional Thoughts
    - We know people have a variety of different backgrounds. Coding is an important part of the work we do, so you can expect the job to involve a lot of this kind of work. But we also look at the application as a whole, and really value people interested in learning with us! So if you have another A+ skill that's relevant, even if you have less coding experience, don't get discouraged: if you're willing to learn the skills you need to submit the application, we'd love to hear from you.

## What You'll Need

### Data
All the required data can be found in [/data](./data) in `.feather` files.
- We present the data to you in [feather](https://github.com/wesm/feather) files because we commonly use this format in the lab.
- Helpful functions for reading these binary files:
    - R: feather package ([docs](https://cran.r-project.org/web/packages/feather/feather.pdf))
        - `read_feather()`: Read feather files.
    - python: pandas library ([docs](https://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_feather.html))
        - `pandas.read_feather()`: load a feather-format object from the file path

Documentation for these
files can be found in the [data dictionary](https://github.com/synthetichealth/synthea/wiki/CSV-File-Data-Dictionary)
kindly provided by the team behind SyntheticMass.

### Software
Here are a few suggestions as to how to pursue this project and to get a sense
of our lab's technical stack.

#### Programming Language
Our lab's codebase is primarily in R and python. We encourage you to complete the project in either of these languages. (If another language comes more naturally to you, but you still want to submit the project, that's ok -- it will just be a bit harder: for you, to get the data and submit the code, and for us to evaluate your work.)

The easiest way to submit your work are: [Jupyter notebooks](https://jupyter.org/) or [R Markdown](https://rmarkdown.rstudio.com/) documents. But if you have a better idea, you are welcome to submit the data challenge in any way you like.

#### Virtual Environment (optional)
We use [Conda](https://conda.io/docs/index.html) for package management and
environment management. This allows us to reproduce the programming environment and
use the appropriate version of packages.

We have included sample [conda environments](https://conda.io/docs/user-guide/concepts.html#conda-environments) in [/envs](./envs) for R and python. These will provide you with versions of some basic libraries you might find helpful as you get up and running with the provided data.

In order to use these environments, you need [Anaconda](https://www.anaconda.com/).
- To install, view the  [installation documentation](https://docs.anaconda.com/anaconda/install/).

Then, run the shell command:
- for python
    ```
    conda env create -f=./envs/python.yaml
    ```
- for R
    ```
    conda env create -f=./envs/R.yaml
    ```

You should now have a conda environment at your disposal.
- To enter into the environment, use the shell command `source activate labsysmed_project`.
- Once inside the environment
    - You can use R or python as normal, install additional libraries, etc.
    - Export the environment to a yaml file: `conda env export > datachallenge.yaml`
- When finished, you can leave the environment with shell command `source deactivate`.

## Submission

To submit your work, please upload the relevant files including code [here](https://forms.gle/hLJw978LrtstP1F78). Please include a README letting us know how to navigate the submitted files.

## Timing and Contact

We are hiring on a rolling basis for a limited number of positions. If you have
any questions, please email us: systemsmedicinelab@gmail.com.
